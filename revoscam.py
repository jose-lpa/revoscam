import argparse
import logging
import os
import random
import string
import time
from multiprocessing import Pool

from faker import Faker
from requests import Request, Session

from user_agents import agents


fake = Faker('en_GB')


class ResponseError(Exception):
    pass


def make_request(
    endpoint: str,
    form_data: dict,
    session: Session,
    device_agent: str
):
    # Prepare request within the session scope.
    request = Request(
        'POST',
        f'http://uk-revolut.info{endpoint}',
        files=form_data,
        headers={'User-Agent': device_agent}
    )
    prepped = session.prepare_request(request)

    # Send request.
    response = session.send(prepped, timeout=(10, 10))

    if response.status_code == 200:
        logging.debug(f'{endpoint} data sent successfully.')
        logging.debug(prepped.body.decode('utf-8'))
        logging.debug(prepped.headers)
    else:
        logging.error(
            f'{endpoint} data not accepted: (%s) %s',
            response.status_code,
            response.content
        )
        raise ResponseError('Scammers did not accept the submitted data.')


def send_email(session: Session, device_agent: str):
    email_password = ''.join(
        random.choice(string.ascii_lowercase) for i in range(32)
    )

    data = {'_': int(time.time()), 'email_pass': email_password}

    make_request('/api/email_result', data, session, device_agent)


def send_card(session: Session, device_agent: str):
    card = fake.credit_card_full().split('\n')
    name = card[1]
    card_number = card[2].split(' ')[0]
    card_expiry = card[2].split(' ')[1].split('/')
    card_expiry_month = card_expiry[0]
    card_expiry_year = '20' + card_expiry[1]
    card_cvc = card[3].split(' ')[1]
    phone_number = fake.phone_number().replace(' ', '')

    address = fake.address().split('\n')
    if len(address) == 4:
        street = f'{address[0]}, {address[1]}'
        city = f'{address[2]}'
        post_code = f'{address[3]}'
    else:
        street = f'{address[0]}'
        city = f'{address[1]}'
        post_code = f'{address[2]}'

    data = {
        'card_name': name,
        'card_number': card_number,
        'card_exp': f'{card_expiry_month}/{card_expiry_year}',
        'card_cvc': card_cvc,
        '_': int(time.time()),
        'phone': phone_number,
        'passcode': random.randint(1111, 9999),
        'fullname': name,
        'address': street,
        'city': city,
        'state': city,
        'zipcode': post_code,
        'email': fake.email(),
        'dob': fake.date_of_birth().strftime('%d/%m/%Y')
    }

    make_request('/api/card_result', data, session, device_agent)


def send_account(session: Session, device: str, device_agent: str):
    last_digits = ''.join(random.choice(string.digits) for _ in range(4))

    data = {
        '_': int(time.time()),
        'topup': random.randint(50, 1000),
        'transfered': random.randint(130, 10000),
        'last_payment_last4digit': last_digits,
        'last_payment_cvv': ''.join(
            random.choice(string.digits) for _ in range(3)
        ),
        'last_login_device': device,
    }

    make_request('/api/account_result', data, session, device_agent)


def scam_them(requests_number: int):
    device = random.choice(list(agents.keys()))
    device_agent = agents[device]

    for _ in range(requests_number):
        with Session() as session:
            send_card(session, device_agent)
            # time.sleep(random.randint(20, 60))
            send_account(session, device, device_agent)
            # time.sleep(random.randint(20, 60))
            send_email(session, device_agent)

        logging.info('Full dataset submitted (PID %s).', os.getpid())

    logging.info(
        'Process %s finished sending %s datasets', os.getpid(), requests_number
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Fuck scammers by flooding them with fake data.'
    )
    parser.add_argument('--debug', action='store_true')
    parser.add_argument(
        '--processes', type=int,
        help='Number of processes. By default only one process is ran to make '
             'requests. Using this option thus enables multiprocessing.'
    )
    parser.add_argument(
        '--shots', type=int, default=1,
        help='Number of data submissions to be made to scammer. Note that when'
             ' using multiprocessing every process will run this number of '
             'requests. Defaults to 1.'
    )
    args = parser.parse_args()

    # Configure logging.
    logging.basicConfig(
        format='%(asctime)s [%(levelname)s]: %(message)s',
        level=logging.DEBUG if args.debug is True else logging.INFO
    )

    if args.processes:
        with Pool(processes=args.processes) as pool:
            [
                pool.apply_async(scam_them, (args.shots,))
                for _ in range(args.processes)
            ]
            pool.close()
            pool.join()

        logging.info(
            'Successfully submitted %s datasets accross %s processes.',
            args.shots * args.processes, args.processes
        )
    else:
        scam_them(args.shots)
        logging.info('Successfully submitted 1 dataset 👍')
