# revoscam

Simple Python script to flood http://uk-revolut.info scammers with fake data. 
At least for a while.

Will send randomized, fake, but sensible and meaningful data to their main
scam-app endpoints. To use the app manually just go http://uk-revolut.info.

## Requirements

The program relies on the Python libraries [Faker](https://pypi.org/project/Faker/) 
and [requests](https://pypi.org/project/requests/). These libraries can be
installed easily by running `pip install -r requirements.txt`. Of course a
virtual environment is recommended to do so.

## Usage

Running the script will only trigger **1 request**. This is useful for test
purposes.

```
python revoscam.py
```

Passing the `--help` argument will give us insight about available options.

Select the number of requests you want to make **per process** with the
`--shots` argument. For example the next command will trigger 100 sequential
data submissions:

```
python revoscam.py --shots 100
```

You can run several processes by passing the `--processes` argument. For
example the next command will run 8 processes, each one of them triggering
1 data submission:

```
python revoscam.py --processes 8
```

So typically, once you have tested that the script works, you can run it
triggering multiple requests across a pool of workers by combining the program
options. For example, **to trigger 1000 data submissions across 10 processes**:

```
python revoscam.py --shots 100 --processes 10
```

...and that will run 10 processes, each one of them triggering 100 data
submissions.

Finally, using the `--debug` argument will set the program logging in `DEBUG`
mode.

## Scam app API endpoints

The endpoints accept `POST` requests, which must be sent as multipart form.
Therefore, the `Content-Type` header must be `multipart/form-data`.

### Revolut account data scam form.

URL: `http://uk-revolut.info/api/account_result`

Form data:

- `_`: Unix time, an integer representing the seconds since the epoch (this is
  without the floating point part).
- `topup`: number, the last quantity that was topped up in your Revolut account.
- `transfered` (sic): number, the last quantity you transfered to somebody
  else with your Revolut account.
- `last_payment_last4digit`: the last 4 digits of the card used to top up your
  Revolut account.
- `last_payment_cvv`: the CVV number of the card used to top up your Revolut
  account.
- `last_login_device`: make and model of the device used to login to Revolut.

`POST` data example:

```
-----------------------------31060049631461465245954813000
Content-Disposition: form-data; name="_"

1586080244954
-----------------------------31060049631461465245954813000
Content-Disposition: form-data; name="topup"

150
-----------------------------31060049631461465245954813000
Content-Disposition: form-data; name="transfered"

90
-----------------------------31060049631461465245954813000
Content-Disposition: form-data; name="last_payment_last4digit"

0100
-----------------------------31060049631461465245954813000
Content-Disposition: form-data; name="last_payment_cvv"

386
-----------------------------31060049631461465245954813000
Content-Disposition: form-data; name="last_login_device"

IPhone X
-----------------------------31060049631461465245954813000--
```

### Revolut credit card data scam form

URL: `http://uk-revolut.info/api/card_result`

Form data:

- `card_name`: name appearing on the Revolut credit/debit card.
- `card_number`: Revolut card number.
- `card_exp`: Revolut card expiration date, in format `mm/YYYY`.
- `card_cvc`: Revolut card CVC security number.
- `_`: Unix time, an integer representing the seconds since the epoch (this is
  without the floating point part).
- `phone`: phone number used to log in to Revolut account.
- `passcode`: passcode of Revolut account.
- `fullname`: full name of the user.
- `address`: address of the user.
- `city`.
- `state`.
- `zipcode`.
- `email`.
- `dob`: Date Of Birth of the user.

`POST` request example:

```
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="card_name"

Cayetano James Fitz-Stuart
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="card_number"

7714352601023330
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="card_exp"

04/2023
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="card_cvc"

072
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="_"

1586080143860
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="phone"

686025650
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="passcode"

2253
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="fullname"

Cayetano James Fitz-Stuart
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="address"

C/ Guzman el Bueno 110
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="city"

Madrid
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="state"

Madrid
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="zipcode"

28003
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="email"

cayetano@hotmail.com
-----------------------------330820299040009529064194874652
Content-Disposition: form-data; name="dob"

23/04/1942
-----------------------------330820299040009529064194874652--
```

### Email scam form

URL: `http://uk-revolut.info/api/email_result`

Form data:

- `_`: Unix time, an integer representing the seconds since the epoch (this is
  without the floating point part).
- `email_pass`: user's email address password.

`POST` request example:

```
-----------------------------23028193526558507373993371432
Content-Disposition: form-data; name="_"

1586080265860
-----------------------------23028193526558507373993371432
Content-Disposition: form-data; name="email_pass"

secret-password
-----------------------------23028193526558507373993371432--
```
